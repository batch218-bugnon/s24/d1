// console.log("ES6 Updates");

// ECMAScript is the standard that is used to create implementations of the language, one which is JavaScript.
// where all browser vendor can implement (Apple, Google, Microsoft, Mozilla, etc.);

// New features of JavaScript 

//  [SECTION] Exponent Operator 

console.log("ES6 Updates");
console.log("-----------");
console.log("=> Exponent Operator");

// using Math Object Methods
const firstNum = Math.pow(8, 3); // 8 raise to 3 
console.log(firstNum);

// Using the exponent operator
const secondNum = 8 ** 3; // 8 raise to 3
console.log(secondNum);



// [SECTION] Template Literals 

/*
	- Allows to write string without using the concatenation operator (+).
	- ${} is called placeholder when using template literals, and we can input variables or expression.

*/

console.log("-----------");
console.log("=> Template Literals");

let name = "John";

// pre-template literals
let message = "Hello " + name + "! Welcome to programming"; // Hello John! Welcome to programming
console.log("Message without template literals: ");
console.log(message);


// String using template literals
// Using backticks (``) instead of ("") or ('')
// we use ${} to access a value or variable
message = `Hello ${name}! Welcome to programming`;
console.log("Message with template literals: ");
console.log(message);


// [SECTION] Array Destructuring 

// It allows us to name array elements with variableNames instead of using the index numbers.
/*
	- Syntax:
		let/const [variableName1, variableName2, variableName3] = arrayName;
*/

/*
	Syntax: 
		let/const [variableName1, variableName2, variableName3] = arrayName;
*/

console.log("-----------");
console.log("=> Array Destructuring");

const fullName = ["Juan", "Dela", "Cruz"];

// Pre-Array Destructuring 
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);


console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you!`);
console.log("-----------");


// Array Destructuring
// Variable naming for array destructuring is based on the developers choice 

const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);


console.log(`Hello ${firstName} ${middleName} ${lastName}! It is nice to meet you!`);


// [SECTION] Object Destructuring 
/*
 Shortens the syntax for accessing properties form object 
 The difference with array destructuring, this should be exact property name. 

 Syntax: 
 	let/const {propertyNameA, propertyNameB, propertyNameC} = object;

*/


console.log("-----------");
console.log("=> Object Destructuring");

const person = {
	givenName: "Jane",
	mName: "Dela",
	surname: "Cruz"
}

// Pre-object Destructuring 
console.log(person.givenName);
console.log(person.mName);
console.log(person.surname);

console.log(`Hello ${person.givenName} ${person.mName} ${person.surname}! It is good to see you`);


console.log("-----------");
// Object Destructuring

const {givenName, mName, surname} = person;

console.log(givenName);
console.log(mName);
console.log(surname);

console.log(`Hello  ${givenName} ${mName} ${surname}! It is good to see you`);


console.log("-----------");
console.log("=> Arrow Functions");


// [SECTION] Arrow Functions 

/*
	- Compact alternative syntax to traditional functions 
	- Useful for code snippets where creating functions will not be reused in any other portion of code
	- This will work with function expression (s17)

	example of funcExpression 

	let funcExpression = function funcName(){
	console.log("Hello from the other size");
	}
	funcExpression();
*/

/*
	syntax:
		let/const variableName = (parameter) => {
			// code to execute;
		}

		//invocation
		variableName(argument);
*/

const hello = () => {
	console.log("Hello from the other side");
}

hello();


console.log("-----------");
// Pre-arrow function
const students = ["John", "Jane", "Judy"];

students.forEach(function(student){
	console.log(`${student} is a student`);
});




/*
	syntax:
		arrayName.arrayMethod(parameter) =>
			// code to execute;
		);

*/

console.log("-----------");
// forEach methos with the use of arrow function
students.forEach((student) =>
	console.log(`${student} is a student.`)

	);

// anonymous function - a function that has no function name


console.log("-----------");
console.log("=> Implicit Return using arrow functions");

// There are instances when you can omit the "return" statement.
// Implicit return means - Returns the statement/value even without the return keyword;
/*
	const add = (x, y) =>{
	return x + y;
 	}
*/

/*
	syntax: 
		let/const variableName = (parameters) => code to eexecute
*/

const add = (x,y) => x + y;

let total = add(1,2);
console.log(total); // 3


/* 

function add(x,y){
	return x + y;
}

let total = add(1,2);
console.log(total); // 3
*/


// [SECTION] Default Function Agrument Value
// Provides a default argument value if none is provided when the function is invoked.

// const greet = (name) => `Good morning, ${name}`;
// console.log(greet()); // no argument provided will result to undefined

const greet = (name = "User") => `Good morning ${name}`;
console.log(greet());
console.log(greet("John"));

console.log("-----------");
console.log("=> Class-based Object Blueprint");
// [SECTION] Class-Based Object Blueprint
// Another approach in creating an object with key and value;
// Allows creation/instantiation of object using classes as blueprints.

// the "constructor" is a special method of a class for creating/Initializing object for that clas.
/*
- Syntax:
		class className{
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
			// insert function outside our constructor
		}
*/


class car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}


// "new" operator creates/instantiates a new object with the given argument as value of it's property.

const myCar = new car();
console.log(myCar);

// Reassigning value of each property

myCar.brand = "ford";
myCar.name = "Ranger raptor";
myCar.year = 2021;
console.log(myCar);





























